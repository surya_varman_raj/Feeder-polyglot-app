stages:
  - build
  - test
  - dockerize
  - deploy

variables:
  AWS_DEFAULT_REGION: us-east-1
  ECR_REPOSITORY: 123456789012.dkr.ecr.us-east-1.amazonaws.com/my-ecr-repo
  EKS_CLUSTER_NAME: my-eks-cluster
  KUBE_NAMESPACE: my-kube-namespace

before_script:
  # Setup AWS CLI and Kubernetes tools
  - apt-get update && apt-get install -y python3-pip
  - pip3 install awscli
  - aws eks --region $AWS_DEFAULT_REGION update-kubeconfig --name $EKS_CLUSTER_NAME
  - apt-get install -y kubectl

build:
  stage: build
  script:
    - echo "Building microservices"
    # Replace with build commands for each microservice
    - echo "Building Login Service..."
    - echo "Building Posts Service..."
    - echo "Building Feed Service..."

test:
  stage: test
  script:
    - echo "Running tests"
    # Replace with test commands for each microservice
    - echo "Testing Login Service..."
    - echo "Testing Posts Service..."
    - echo "Testing Feed Service..."

dockerize:
  stage: dockerize
  script:
    - echo $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $ECR_REPOSITORY
    - docker build -t $ECR_REPOSITORY/login-service:$CI_COMMIT_REF_SLUG -f path/to/Dockerfile.login .
    - docker push $ECR_REPOSITORY/login-service:$CI_COMMIT_REF_SLUG
    - docker build -t $ECR_REPOSITORY/posts-service:$CI_COMMIT_REF_SLUG -f path/to/Dockerfile.posts .
    - docker push $ECR_REPOSITORY/posts-service:$CI_COMMIT_REF_SLUG
    - docker build -t $ECR_REPOSITORY/feed-service:$CI_COMMIT_REF_SLUG -f path/to/Dockerfile.feed .
    - docker push $ECR_REPOSITORY/feed-service:$CI_COMMIT_REF_SLUG

deploy:
  stage: deploy
  script:
    - kubectl set image deployment/login-service-deployment login-service=$ECR_REPOSITORY/login-service:$CI_COMMIT_REF_SLUG --namespace=$KUBE_NAMESPACE
    - kubectl set image deployment/posts-service-deployment posts-service=$ECR_REPOSITORY/posts-service:$CI_COMMIT_REF_SLUG --namespace=$KUBE_NAMESPACE
    - kubectl set image deployment/feed-service-deployment feed-service=$ECR_REPOSITORY/feed-service:$CI_COMMIT_REF_SLUG --namespace=$KUBE_NAMESPACE
