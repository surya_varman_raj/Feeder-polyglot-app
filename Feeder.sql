-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 18, 2023 at 11:06 AM
-- Server version: 5.7.40
-- PHP Version: 8.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `twitter`
--

-- --------------------------------------------------------

--
-- Table structure for table `tweets`
--

DROP TABLE IF EXISTS `tweets`;
CREATE TABLE IF NOT EXISTS `tweets` (
  `tweet_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(500) NOT NULL,
  `useremail` varchar(500) NOT NULL,
  `tweet` text NOT NULL,
  PRIMARY KEY (`tweet_id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tweets`
--

INSERT INTO `tweets` (`tweet_id`, `username`, `useremail`, `tweet`) VALUES
(1, 'user one', 'userone@gmail.com', ' \"Wow, the weather today is absolutely gorgeous! It\'s the perfect day to soak up some sunshine and enjoy the outdoors. #sunnydays #lovetheweather\"\r\n'),
(2, 'user two', 'usertwo@gmail.com', '\"Who needs a vacay to the tropics when we have this amazing weather right here? It\'s like a little slice of paradise in our own backyard! #sunnybliss #enjoyingtheday\"\r\n'),
(3, 'user three', 'userthree@gmail.com', ' \"Wow, the weather today is absolutely gorgeous! It\'s the perfect day to soak up some sunshine and enjoy the outdoors. #sunnydays #lovetheweather\"\r\n'),
(4, 'user four', 'userfour@gmail.com', '\"Who needs a vacay to the tropics when we have this amazing weather right here? It\'s like a little slice of paradise in our own backyard! #sunnybliss #enjoyingtheday\"\r\n'),
(7, 'user five', 'userfive@gmail.com', '\"Mother Nature is giving us quite the show today! From sunny skies to unexpected rain showers, she\'s keeping us on our toes. Embracing all the weather surprises this day brings! #weatherfun #naturelover\"\r\n'),
(8, 'user six', 'usersix@gmail.com', '\"Did you know that calculus and algebra, two fundamental branches of mathematics, have shaped our understanding of science, engineering, and technology? Discover the beauty and power of maths, the best subject in the world! #Mathematics #Calculus #Algebra #Education\"\r\n'),
(9, 'user seven', 'userseven@gmail.com', '\"Why is maths considered the best subject in the world? Not only does it help us solve everyday problems, but it also trains our minds to think critically and logically. Dive into the fascinating world of numbers and equations and unlock endless possibilities! #Mathematics #Education\"\r\n\r\n'),
(10, 'user eight', 'usereight@gmail.com', '\"Are you ready to embark on a journey into the realm of numbers? Maths offers us a universal language to understand and analyze the world around us. From calculating the trajectory of a rocket to predicting stock market trends, this subject holds infinite wonders! #Maths #Education\"\r\n'),
(11, 'user nine', 'usernine@gmail.com', '\r\n\"No matter how tough things get, keep fighting hard and never give up. Success comes to those who persevere! #NeverGiveUp #FightHard\"\r\n'),
(12, 'user ten', 'userten@gmail.com', ' \"Success is not achieved overnight. It\'s the result of continuous hard work and dedication. Keep pushing through, remain focused, and never give up on your dreams. You\'ve got this! #WorkHard #NeverGiveUp\"\r\n'),
(13, 'user eleven', 'usereleven@gmail.com', '\"Life is full of challenges, but don\'t let them defeat you. Embrace them as opportunities to grow and learn. With determination, resilience, and a strong work ethic, there\'s no limit to what you can achieve. Keep pushing forward!  #FightHard #WorkHard #NeverGiveUp\"'),
(53, 'user eleven', 'usereleven@gmail.com', '\"Mahendhar thopu\"'),
(52, 'user eleven', 'usereleven@gmail.com', '\"cristiano ronaldo\"'),
(51, 'user eleven', 'usereleven@gmail.com', '\"This is eleven\"');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
