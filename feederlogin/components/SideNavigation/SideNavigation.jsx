import useUser from '@/hooks/useUser';
import Link from 'next/link';

export default function SideNavigation() {
  const { currentAccount, logout } = useUser();
  return (
    <div className="w-1/5 text-white py-4 h-auto">
      <div className="px-4">
        <svg
          viewBox="0 0 24 24"
          className="h-9 w-9 text-white"
          fill="currentColor"
        >
          
        </svg>
      </div>

      <nav className="mt-5 px-2">
       
        <a
          href="#"
          className="mt-1 group flex items-center px-2 py-2 text-base leading-6 font-semibold rounded-full hover:bg-gray-900 text-white"
        >
          <svg
            className="mr-4 h-6 w-6"
            fill="none"
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            stroke="currentColor"
            viewBox="0 0 24 24"
          >
            <path d="M7 20l4-16m2 16l4-16M6 9h14M4 15h14"></path>
          </svg>
          Explore
        </a>
       
      
      </nav>

      <div className="flex-shrink-0 flex-col gap-4 flex hover:bg-blue-00 rounded-full p-4 mt-12 mr-2">
        <p className="capitalize">Hi, {currentAccount?.name}!</p>
        <button
          href="#"
          className="flex-shrink-0 group block border-none bg-transparent"
          onClick={logout}
        >
          <div className="flex items-start flex-col">
            <div className="flex gap-4 items-center">
              <p className="text-base leading-6 font-medium text-white">
                Logout
              </p>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15m3 0l3-3m0 0l-3-3m3 3H9"
                />
              </svg>
            </div>
          </div>
        </button>
      </div>
    </div>
  );
}