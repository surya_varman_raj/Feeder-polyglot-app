import React from "react";

import SideNavigation from '@/components/SideNavigation';
import Spinner from "@/components/Spinner";
import useUser from "@/hooks/useUser";
import { useRouter } from "next/router";

export default function MainLayout({ children }) {
  const { currentAccount, isLoadingAccount } = useUser();
  const router = useRouter();
  // const token = localStorage.getItem('token');

  const token= typeof window !== "undefined" ? window.localStorage.getItem('token') : false

  React.useEffect(() => {
    if (!token) {
      // If there is no account present and we finish the get account request redirect to login
      router.push('/auth/signin');
    }
  }, [currentAccount, router, isLoadingAccount]);

  // if (!currentAccount) {
  //   // While there is no account show a spinner
  //   return (
  //     <div className="bg-black w-full min-h-screen flex items-center justify-center">
  //       <Spinner />
  //     </div>
  //   );
  // }

  return (
    <div className="bg-black">
      <div className="min-h-screen container mx-auto">
        <div className="flex justify-center">
          <SideNavigation />

          {children}

          <div className=" w-1/4 p-5">
            <div className="relative text-gray-300 pb-0 w-full">
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
