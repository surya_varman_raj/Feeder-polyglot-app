import React from 'react';
import axios from 'axios';

import useUser from '@/hooks/useUser';

export default function CreateTweetForm({ onTweetCreated }) {
  const { currentAccount } = useUser();

  const [tweetForm, setTweetForm] = React.useState({
    text: '',
  });

  const onChangeInput = (event) => {
    const {
      target: { name, value },
    } = event;
    setTweetForm((currTweetForm) => ({ ...currTweetForm, [name]: value }));
  };

  const onSubmit = async (event) => {
    event.preventDefault();

    try {
      const resp = await axios.post('http://twitterphp.test/', {
        text: tweetForm.text,
      })
      .then(function (response) {
        console.log(response.data.lastInsertedId);
        console.log(response.data.message);
        const tweetId = response.data.lastInsertedId;
        const tweet = {
          tweet_id: tweetId,  // Ensure that the property name matches your existing data structure
          useremail: 'usereleven@gmail.com',
          username: 'user eleven',
          tweet: response.data.message,
        };
        setTweetForm({text: ''})
        onTweetCreated(tweet);
      })
      .catch(function (error) {
        console.log(error);
      });

      
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <form onSubmit={onSubmit}>
      <div className="flex">
        <div className="flex-1 px-6 pt-2 mt-2">
          <textarea
            onChange={onChangeInput}
            value={tweetForm.text}
            name="text"
            className=" bg-transparent outline-none focus:ring-1 focus:ring-gray-800 rounded-lg p-3 text-white placholder:text-gray-400 font-medium text-lg w-full"
            rows="2"
            cols="50"
            placeholder="What's happening?"
          ></textarea>
        </div>
      </div>

      <div className="flex">
        <div className="w-10"></div>

        <div className="w-64 px-2">
          <div className="flex items-center">
           
          </div>
        </div>

        <div className="flex-1">
          <button
            type="submit"
            className="bg-blue-400 mt-5 hover:bg-blue-600 text-white font-bold py-2 px-8 rounded-full mr-8 float-right mb-3"
          >
            Post Message
          </button>
        </div>
      </div>
    </form>
  );
}
