import React from 'react';

import { useRouter } from 'next/router';
import { FETCH_STATUS } from '@/utils/constants';

export default function useUser() {
  const [someCurrentAccount, setCurrentAccount] = React.useState();
  const [accountStatus, setAccountStatus] = React.useState(
    FETCH_STATUS.LOADING
  );
  const router = useRouter();
  let currentAccount = {
    $id: '123456',
    name: 'user eleven',
    email: 'usereleven@gmail.com',
  };

  const getSession = async () => {
    setAccountStatus(FETCH_STATUS.LOADING);
    try {
      setAccountStatus(FETCH_STATUS.SUCCESS);
    } catch (error) {
      console.log(error);
      setAccountStatus(FETCH_STATUS.FAIL);
    } finally {
      setCurrentAccount(currentAccount);
    }
  };

  const logout = async () => {
    // const promise = await account.deleteSession('current');
    // setCurrentAccount(null)
    // let currentAccount = null;
    window.localStorage.removeItem('token');
    router.push('/auth/signin')
  };

  React.useEffect(() => {
    getSession();
  }, []);

  return {
    currentAccount,
    isLoadingAccount: accountStatus === FETCH_STATUS.LOADING,
    logout,
  };
}
