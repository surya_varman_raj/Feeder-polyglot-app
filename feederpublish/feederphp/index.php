<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

include 'DbConnect.php';
$objDb = new DbConnect;

$conn = $objDb->connect();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $query = "SELECT * FROM tweets order by tweet_id desc";
    $stmt = $conn->prepare($query);

    if ($stmt->execute()) {
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($data);
        return;
    } else {
        echo "Error: " . $stmt->errorInfo()[2];
        return;
    }

}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $input = json_decode(file_get_contents('php://input'), true);

    // if (!isset($_POST['text'])) {
    //     echo "Error: 'text' field is missing in the request.";
    //     return;
    // }

    $tweet = '"'.$input['text'].'"';
    $username = 'user eleven';
    $useremail  = 'usereleven@gmail.com';

    $query = "INSERT INTO tweets (tweet, username, useremail) VALUES (:tweet, :username, :useremail)";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(':tweet', $tweet);
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':useremail', $useremail);

    $response = [];

    if ($stmt->execute()) {
        $lastInsertedId = $conn->lastInsertId();
        $response = ['status' => 200, 'message' => $tweet, 'lastInsertedId' => $lastInsertedId];
    } else {
        $response = ['status' => 500, 'message' => "Error: " . $stmt->errorInfo()[2]];
    }

    echo json_encode($response);
    return;
}